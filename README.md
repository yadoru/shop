# SHOP

Repository about shops in local area, currently in development.

# REQUIREMENTS
Docker-compose is required to run the repository
(Informations about instalation process can be found [Docker-compose][reference-id-for-Docker-compose]
[reference-id-for-Docker-compose]:https://docs.docker.com/compose/install/ 

# HOW TO RUN

from within project folder use "docker-compose up" command

then run startup.sh script
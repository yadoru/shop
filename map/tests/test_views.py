# this test may not work properly if it is ran around 23:00 - 00:00 o'clock

from django.contrib.auth.models import User
from django.db.models import Q
from django.test import TestCase, Client
from django.urls import reverse

# from map.models import Shop, Town
from map.models import Shop

c = Client()

class IndexViewTest(TestCase):
    def test_IndexViewGet(self):
        response = c.get(reverse("frontpage"))
        self.assertEqual(response.status_code, 200)

    def test_IndexViewGetAjax(self):
        response = c.get(reverse("frontpage"), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

    def testIndexViewPost(self):
        response = c.post(reverse("frontpage"))
        self.assertEqual(response.status_code, 405)

    def testIndexViewPostAjax(self):
        response = c.post(reverse("frontpage"))
        self.assertEqual(response.status_code, 405)

class SendLocationViewTest(TestCase):
    def test_SendLocationViewGet(self):
        response = c.get(reverse('get_location'))
        self.assertEqual(response.status_code, 200)

    def test_SendLocationViewGetAjax(self):
        response = c.get(reverse('get_location'))
        self.assertEqual(response.status_code, 200)

    def test_SendLocationViewPost(self):
        response = c.post(reverse('get_location'))
        self.assertEqual(response.status_code, 302)

    def test_SendLocationViewPostAjax(self):
        response = c.get(reverse('get_location'))
        self.assertContains(response, "redirect")

class NearbyViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('testuser', 'test@gmail.com', 'Kakegurui18', )
        # self.town = Town.objects.create(town='Equate')
        # self.opened = Shop.objects.create(name='Atlas', town=self.town, address='Letho 20', opening_hour='0:00',
        #                              closing_hour='23:59', geo_point='POINT (50 50)', owner=self.user)
        # self.closed = Shop.objects.create(name='Atlanta', town=self.town, address='Letho 21', opening_hour='0:00',
        #                              closing_hour='00:01', geo_point='POINT (50 50.0001)', owner=self.user)
        # self.weirdhour = Shop.objects.create(name='Atlanthida', town=self.town, address='Letho 24', opening_hour='23:59',
        #                              closing_hour='23:58', geo_point='POINT (50 50.001)', owner=self.user)
        # self.weirdhour2 = Shop.objects.create(name='Atlantha', town=self.town, address='Letho 243', opening_hour='00:02',
        #                              closing_hour='00:01', geo_point='POINT (50 50.001)', owner=self.user)
        # self.faraway = Shop.objects.create(name='Atlanthis', town=self.town, address='Maroko 21', opening_hour='00:00',
        #                              closing_hour='23:59', geo_point='POINT (60 50.0001)', owner=self.user)
        self.opened = Shop.objects.create(name='Atlas', opening_hour='0:00',
                                     closing_hour='23:59', geo_point='POINT (50 50)', owner=self.user)
        self.closed = Shop.objects.create(name='Atlanta', opening_hour='0:00',
                                     closing_hour='00:01', geo_point='POINT (50 50.0001)', owner=self.user)
        self.weirdhour = Shop.objects.create(name='Atlanthida', opening_hour='23:59',
                                     closing_hour='23:58', geo_point='POINT (50 50.001)', owner=self.user)
        self.weirdhour2 = Shop.objects.create(name='Atlantha', opening_hour='00:02',
                                     closing_hour='00:01', geo_point='POINT (50 50.001)', owner=self.user)
        self.faraway = Shop.objects.create(name='Atlanthis', opening_hour='00:00',
                                     closing_hour='23:59', geo_point='POINT (60 50.0001)', owner=self.user)
        self.close = Shop.objects.all().filter(Q(id=self.opened.id) | Q(id=self.closed.id) |
                                               Q(id=self.weirdhour.id) | Q(id=self.weirdhour2.id))
        self.close_open = Shop.objects.all().filter(Q(id=self.opened.id) | Q(id=self.weirdhour.id) | \
                                                    Q(id=self.weirdhour2.id))


    def test_NearbyViewGetEmpty(self):
        response = c.get(reverse('nearby'))
        self.assertEqual(response.status_code, 302)

    def test_NearbyViewGetAjaxEmpty(self):
        response = c.get(reverse('nearby'), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, "redirect")

    def test_NearbyViewPost(self):
        response = c.post(reverse('nearby'))
        self.assertEqual(response.status_code, 405)

    def test_NearbyViewPostAjax(self):
        response = c.post(reverse('nearby'), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 405)

    def test_NearbyViewGetNoDistance(self):
        response = c.get(reverse('nearby')+"?location=POINT(50 50)")
        self.assertEqual(response.status_code, 302)

    def test_NearbyViewGetAjaxNoDistance(self):
        response = c.get(reverse('nearby')+"?location=POINT(50 50)", HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, "redirect")

    def test_NearbyViewGetNoLocation(self):
        response = c.get(reverse('nearby')+"?distance=500")
        self.assertEqual(response.status_code, 302)

    def test_NearbyViewGetAjaxNoLocation(self):
        response = c.get(reverse('nearby')+"?distance=500", HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, "redirect")

    def test_NearbyViewGet(self):
        response = c.get(reverse('nearby') + "?location=POINT(50 50)&distance=500")
        self.assertEqual(response.status_code, 200)

    def test_NearbyViewShopsNearby(self):
        response = c.get(reverse('nearby') + "?location=POINT(50 50)&distance=500",
                         HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(list(response.context_data['object_list']), list(self.close))

    def test_NearbyViewShopsOpenedNearby(self):
            response = c.get(reverse('nearby') + "?location=POINT(50 50)&distance=500&opened=true",
                             HTTP_X_REQUESTED_WITH='XMLHttpRequest')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(list(response.context_data['object_list']), list(self.close_open))

class SendTownViewTest(TestCase):
    def test_SendTownViewGet(self):
        response = c.get(reverse('get_town'))
        self.assertEqual(response.status_code, 200)

    def test_SendTownViewGetAjax(self):
        response = c.get(reverse('get_town'))
        self.assertEqual(response.status_code, 200)

    def test_SendTownViewPost(self):
        response = c.post(reverse('get_town'))
        self.assertEqual(response.status_code, 302)

    def test_SendTownViewPostAjax(self):
        response = c.get(reverse('get_town'))
        self.assertContains(response, "redirect")
        
# class TownViewTest(TestCase):
#     def setUp(self):
#         self.user = User.objects.create_user('testuser', 'test@gmail.com', 'Kakegurui18', )
#         self.town = Town.objects.create(town='Equate')
#         self.town2 = Town.objects.create(town='Eqiote')
#         self.opened = Shop.objects.create(name='Atlas', town=self.town, address='Letho 20', opening_hour='0:00',
#                                      closing_hour='23:59', geo_point='POINT (50 50)', owner=self.user)
#         self.closed = Shop.objects.create(name='Atlanta', town=self.town, address='Letho 21', opening_hour='0:00',
#                                      closing_hour='00:01', geo_point='POINT (50 50.0001)', owner=self.user)
#         self.weirdhour = Shop.objects.create(name='Atlanthida', town=self.town, address='Letho 24', opening_hour='23:59',
#                                      closing_hour='23:58', geo_point='POINT (50 50.001)', owner=self.user)
#         self.weirdhour2 = Shop.objects.create(name='Atlantha', town=self.town, address='Letho 243', opening_hour='00:02',
#                                      closing_hour='00:01', geo_point='POINT (50 50.001)', owner=self.user)
#         self.faraway = Shop.objects.create(name='Atlanthis', town=self.town2, address='Maroko 21', opening_hour='00:00',
#                                      closing_hour='23:59', geo_point='POINT (60 50.0001)', owner=self.user)
#         self.close = Shop.objects.all().filter(Q(id=self.opened.id) | Q(id=self.closed.id) |
#                                                Q(id=self.weirdhour.id) | Q(id=self.weirdhour2.id)).order_by('name')
#         self.close_open = Shop.objects.all().filter(Q(id=self.opened.id) | Q(id=self.weirdhour.id) |
#                                                     Q(id=self.weirdhour2.id)).order_by('name')
#
#
#     def test_TownViewGetEmpty(self):
#         response = c.get(reverse('town'))
#         self.assertEqual(response.status_code, 302)
#
#     def test_TownViewGetAjaxEmpty(self):
#         response = c.get(reverse('town'), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
#         self.assertContains(response, "redirect")
#
#     def test_TownViewPost(self):
#         response = c.post(reverse('town'))
#         self.assertEqual(response.status_code, 405)
#
#     def test_TownViewPostAjax(self):
#         response = c.post(reverse('town'), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
#         self.assertEqual(response.status_code, 405)
#
#
#     def test_TownViewGet(self):
#         response = c.get(reverse('town') + "?town=Equate")
#         self.assertEqual(response.status_code, 200)
#
#     def test_TownViewShopsNearby(self):
#         response = c.get(reverse('town') + "?town=Equate",
#                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
#         self.assertEqual(response.status_code, 200)
#         self.assertEqual(list(response.context_data['object_list']), list(self.close))
#
#     def test_TownViewShopsOpenedNearby(self):
#             response = c.get(reverse('town') + "?town=Equate&opened=true",
#                              HTTP_X_REQUESTED_WITH='XMLHttpRequest')
#             self.assertEqual(response.status_code, 200)
#             self.assertEqual(list(response.context_data['object_list']), list(self.close_open))

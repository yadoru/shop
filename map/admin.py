from django.contrib import admin
from django.contrib.gis.db import models
from mapwidgets import GooglePointFieldWidget

# from map.models import Town, Sundays
from map.models import Sundays
from .models import Shop

class ShopAdmin(admin.ModelAdmin):
    # list_display = ("name", 'owner', 'town')
    list_display = ("name", "owner")
    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget}
    }

# class TownAdmin(admin.ModelAdmin):
#     list_display = ('town',)

class SundaysAdmin(admin.ModelAdmin):
    list_display = ('date', 'tradable')


admin.site.register(Shop, ShopAdmin)
# admin.site.register(Town, TownAdmin)
admin.site.register(Sundays, SundaysAdmin)

# Register your models here.

from django.conf import settings
from django.contrib.gis.db.models import PointField
from django.db import models
from django.contrib.auth.models import User

from rest_framework import serializers


class Shop(models.Model):
    name = models.CharField(verbose_name='nazwa', max_length=100)
    # town = models.ForeignKey('Town', verbose_name='miasto', on_delete=models.CASCADE, related_name='shops')
    # address = models.CharField(verbose_name='adres', max_length=100)
    opening_hour = models.TimeField(verbose_name='otwarcie')
    closing_hour = models.TimeField(verbose_name='zamkniecie')
    photo = models.ImageField(verbose_name='zdjecie', upload_to='photos', blank=True)
    geo_point = PointField(verbose_name='polozenie')
    owner = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='właściciel', related_name='shops')
    created = models.DateTimeField(auto_now_add=True, verbose_name='data dodania')
    modified = models.DateTimeField(auto_now=True, verbose_name='data modyfikacji')

    def __str__(self):
        return self.name

# class Town(models.Model):
#     town = models.CharField(verbose_name='miasto', max_length=50, unique=True)
#
#     def __str__(self):
#         return self.town

class Sundays(models.Model):
    date = models.DateField(verbose_name='Data', unique=True)
    tradable = models.BooleanField(verbose_name='Jest otwarty?', default='false')

    def __str__(self):
        return str(self.date)


# class TownSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Town
#         fields = ('town',)
#
# class CreatableSlugRelatedField(serializers.SlugRelatedField):
#
#     def to_internal_value(self, data):
#         try:
#             return self.get_queryset().get_or_create(**{self.slug_field: data})[0]
#         except ObjectDoesNotExist:
#             self.fail('does_not_exist', slug_name=self.slug_field, value=smart_text(data))
#         except (TypeError, ValueError):
#             self.fail('invalid')

class ShopSerializer(serializers.HyperlinkedModelSerializer):

    # town = CreatableSlugRelatedField(slug_field='town', queryset=Town.objects.all())
    ## town_send = serializers.CharField(max_length=50, write_only='True')

    class Meta:
        model = Shop
        # fields = ('name', 'address', 'opening_hour', 'closing_hour', 'owner', 'geo_point', 'town', 'url')
        read_only_fields = ('owner',)
        fields = ('name', 'opening_hour', 'closing_hour', 'owner', 'geo_point', 'url')



    def create(self, validated_data):
        request = self.context['request']
        owner = request.user
        # town = validated_data.pop('town')
        # town, exist = Town.objects.get_or_create(town=town)
        # shop = Shop.objects.create(owner=owner, town=town, **validated_data)
        return shop

    def update(self, instance, validated_data):
        instance.geo_point = validated_data.get('geo_point', instance.geo_point)
        instance.name = validated_data.get('name', instance.name)
        # instance.address = validated_data.get('address', instance.address)
        # if(validated_data.get('town')):
        #     town = validated_data.pop('town')
        #     town, exist = Town.objects.get_or_create(town=town)
        # instance.town = town
        # town = validated_data.get('town', instance.town)
        instance.opening_hour = validated_data.get('opening_hour', instance.opening_hour)
        instance.closing_hour = validated_data.get('closing_hour', instance.closing_hour)
        instance.save()
        return instance

class SundaysSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Sundays
        fields = ('date', 'tradable')


# Generated by Django 2.0.4 on 2018-04-28 09:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0009_auto_20180426_1340'),
    ]

    operations = [
        migrations.CreateModel(
            name='Town',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('town', models.CharField(max_length=50, verbose_name='miasto')),
            ],
        ),
        migrations.AlterField(
            model_name='shop',
            name='town',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='map.Town', verbose_name='miasto'),
        ),
    ]

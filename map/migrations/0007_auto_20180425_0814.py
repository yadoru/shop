# Generated by Django 2.0.4 on 2018-04-25 08:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0006_shop_adress'),
    ]

    operations = [
        migrations.RenameField(
            model_name='shop',
            old_name='adress',
            new_name='address',
        ),
    ]

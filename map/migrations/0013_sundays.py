# Generated by Django 2.0.5 on 2018-05-04 12:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0012_auto_20180502_1827'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sundays',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Data')),
                ('tradable', models.BooleanField(default='false', verbose_name='Jest otwarty?')),
            ],
        ),
    ]

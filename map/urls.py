from django.urls import path

from . import views


urlpatterns = [
    path('', views.IndexView.as_view(), name='frontpage'),
    path('znajdz-mnie/', views.SendLocationView.as_view(), name='get_location'),
    # path('miasto/', views.SendTownView.as_view(), name='get_town'),
    path('w-okolicy/', views.NearbyView.as_view(), name='nearby'),
    path('znajdz-sklep/', views.GetPointView.as_view(), name='get_point'),
    path('sklepy/', views.ShowShopsView.as_view(), name='shops'),
    # path('w-miescie/', views.TownView.as_view(), name='town'),
    # path('autocomplete/', views.AutocompleteView.as_view(), name='autocomplete'),
]

from django.contrib.gis.forms import PointField
from django.core.validators import MinValueValidator, MaxValueValidator
from django import forms
from django.forms import HiddenInput
from mapwidgets import GooglePointFieldWidget

# from map.models import Town


class SendLocationForm(forms.Form):
    location = PointField(widget=HiddenInput(), error_messages={'invalid':'Podana lokacja jest nieprawidłowa'})
    distance = forms.IntegerField(validators=[MaxValueValidator(100000), MinValueValidator(500)], error_messages={
        'invalid':'Nieprawidłowy dystans', 'required':'Nie podałeś punktu'
    })

# class SendTownForm(forms.Form):
#     model = Town
#     town = forms.CharField(error_messages={'invalid':'Wpisz prawidłową nazwę miasta',
#                            'required':'Nie podałeś nazwy miasta'})

class MapForm(forms.Form):
    class Meta:
        widgets = {
            'geo_point': GooglePointFieldWidget,
        }
        error_messages = {
            'geo_point': {
                'invalid': 'Nieprawidłowy punkt na mapie',
                'required': 'Nie wybrałeś żadnego punktu na mapie',
            }
        }

    geo_point = PointField()
    distance = forms.IntegerField(validators=[MaxValueValidator(100000), MinValueValidator(500)], error_messages={
        'invalid': 'Nieprawidłowy dystans', 'required': 'Nie podałeś punktu'
    })

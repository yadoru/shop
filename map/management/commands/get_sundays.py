import calendar
from datetime import date

from django.core.management.base import BaseCommand
from map.models import Sundays

class Command(BaseCommand):
    help = 'Dodaj wszystkie niedziele w roku do bazy danych i oznacz pierwszą niedzielę i ostatnia w miesiacu' \
           'jako niedziela handlowa. Nie uwzglednia swiat(w swieta sklepy sa zamkniete).'

    def add_arguments(self, parser):
        parser.add_argument('rok', type=int, nargs='?', default=date.today().year)

    def handle(self, *args, **options):
        rok = options['rok']
        for m in range(1, 13):
            weeks = calendar.monthcalendar(rok, m)
            list_length = len(weeks)
            if(weeks[list_length-1][6]==0):
                list_length-=1
            for i in range(0, list_length):
                day = "{}-{}-{}".format(rok, m, weeks[i][6])
                if(i==0 or i==(list_length-1)):
                    Sundays.objects.update_or_create(date=day, tradable=True)
                else:
                    Sundays.objects.update_or_create(date=day, tradable=False)
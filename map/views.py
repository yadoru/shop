from datetime import datetime

from django.conf import settings
from django.contrib import messages
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.measure import D
from django.db.models import Q, F
from django.http import JsonResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import ListView, FormView
from rest_framework import viewsets
# from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly


# from common.permissions import IsAdminOrReadOnly
# from map.forms import SendTownForm
# from map.models import ShopSerializer, TownSerializer, Sundays
# from .models import Shop, Town
from .forms import MapForm
from map.models import ShopSerializer, Sundays
from .models import Shop
from .forms import SendLocationForm

from common.mixins import AjaxTemplateMixin, AjaxableSelfRedirectOnPostMixIn

google_key = settings.GOOGLE_API_KEY

class IndexView(AjaxTemplateMixin, ListView):
    template_name = 'index.html'
    paginate_by = 8
    model = Sundays
    queryset = Sundays.objects.all().filter(date__gte=datetime.now()).order_by('date')

class SendLocationView(AjaxableSelfRedirectOnPostMixIn, AjaxTemplateMixin, FormView):
    template_name = 'map/get_location.html'
    form_class = SendLocationForm

class NearbyView(AjaxTemplateMixin, ListView):
    template_name = 'map/nearby.html'
    model = Shop
    paginate_by = 10

    def get(self, request, *args, **kwargs):
        if(not self.request.GET.get('location') or not self.request.GET.get('distance')):
            if self.request.is_ajax():
                return JsonResponse({'redirect':reverse('get_location')})
            return redirect(reverse('get_location'))
        sup = super().get(request, *args, **kwargs)
        return sup

    def get_queryset(self):
        request=self.request
        qs = super().get_queryset()
        location = request.GET.get('location')
        max_distance = request.GET.get('distance')
        if(request.GET.get('opened')):
            currentTime = datetime.now().time()
            qs =  qs.filter(Q(opening_hour__lte=F('closing_hour')) &
                            Q(opening_hour__lte=currentTime) &
                            Q(closing_hour__gte=currentTime)) | qs.filter(Q(opening_hour__gt=F('closing_hour')) &
                       Q(opening_hour__lt=currentTime) &
                       Q(closing_hour__lt=currentTime)) | qs.filter(Q(opening_hour__gt=F('closing_hour')) &
                                                                     Q(opening_hour__gt=currentTime) &
                                                                     Q(closing_hour__gt=currentTime))
        if(location):
            point = GEOSGeometry(location)
            qs = qs.filter(geo_point__distance_lte=(point, D(m=max_distance))).\
                annotate(distance=Distance('geo_point', point)).order_by('distance')
        if not qs:
            messages.error(request, 'brak sklepów obok ciebie')
        return qs

    def get_context_data(self):
        request = self.request
        max_distance = int(request.GET.get('distance'))
        context = super().get_context_data()
        if (request.GET.get('opened')):
            currentTime = datetime.now().time()
            context['time'] = currentTime
        context['unit'] = 'm'
        if max_distance>=1000:
            max_distance/=1000
            context['unit'] = 'km'
        context['distance'] = max_distance
        return context

# class SendTownView(AjaxableSelfRedirectOnPostMixIn, AjaxTemplateMixin, FormView):
#     template_name = 'map/get_town.html'
#     form_class = SendTownForm
#
#
# class TownView(AjaxTemplateMixin, ListView):
#     template_name = 'map/town.html'
#     model = Shop
#
#     def get(self, request, *args, **kwargs):
#         if(not self.request.GET.get('town')):
#             if self.request.is_ajax():
#                 return JsonResponse({'redirect':reverse('get_town')})
#             return redirect(reverse('get_town'))
#         sup = super().get(request, *args, **kwargs)
#         return sup
#
#     def get_queryset(self):
#         request=self.request
#         qs = super().get_queryset()
#         if(request.GET.get('opened')):
#             currentTime = datetime.now().time()
#             qs =  qs.filter(Q(opening_hour__lte=F('closing_hour')) &
#                             Q(opening_hour__lte=currentTime) &
#                             Q(closing_hour__gte=currentTime)) | qs.filter(Q(opening_hour__gt=F('closing_hour')) &
#                        Q(opening_hour__lt=currentTime) &
#                        Q(closing_hour__lt=currentTime)) | qs.filter(Q(opening_hour__gt=F('closing_hour')) &
#                                                                      Q(opening_hour__gt=currentTime) &
#                                                                      Q(closing_hour__gt=currentTime))
#         city = request.GET.get('town')
#         if(city):
#             qs = qs.filter(town__town=city)
#         qs.order_by('name')
#         if not qs:
#             messages.error(request, 'Brak sklepów w danym mieście')
#         return qs
#
#     def get_context_data(self, **kwargs):
#         request = self.request
#         city = request.GET.get('town')
#         context = super().get_context_data()
#         if(city):
#             context['town']=city
#         return context

class GetPointView(AjaxableSelfRedirectOnPostMixIn, AjaxTemplateMixin, FormView):
    form_class = MapForm
    template_name = 'map/get_point.html'

class ShowShopsView(AjaxTemplateMixin, ListView):
    template_name = 'map/shops.html'
    model = Shop
    paginate_by = 100

    def get(self, request, *args, **kwargs):
        if(not self.request.GET.get('geo_point') or not self.request.GET.get('distance')):
            if self.request.is_ajax():
                return JsonResponse({'redirect':reverse('get_location')})
            return redirect(reverse('get_location'))
        sup = super().get(request, *args, **kwargs)
        return sup

    def get_queryset(self):
        request=self.request
        qs = super().get_queryset()
        geo_point = request.GET.get('geo_point')
        max_distance = request.GET.get('distance')
        if(request.GET.get('opened')):
            currentTime = datetime.now().time()
            qs =  qs.filter(Q(opening_hour__lte=F('closing_hour')) &
                            Q(opening_hour__lte=currentTime) &
                            Q(closing_hour__gte=currentTime)) | qs.filter(Q(opening_hour__gt=F('closing_hour')) &
                       Q(opening_hour__lt=currentTime) &
                       Q(closing_hour__lt=currentTime)) | qs.filter(Q(opening_hour__gt=F('closing_hour')) &
                                                                     Q(opening_hour__gt=currentTime) &
                                                                     Q(closing_hour__gt=currentTime))
        if(geo_point):
            point = GEOSGeometry(geo_point)
            qs = qs.filter(geo_point__distance_lte=(point, D(m=max_distance))).\
                annotate(distance=Distance('geo_point', point)).order_by('distance')
        if not qs:
            messages.error(request, 'brak sklepów obok ciebie')
        return qs

    def get_context_data(self):
        request = self.request
        point = request.GET.get('geo_point')
        max_distance = int(request.GET.get('distance'))
        context = super().get_context_data()
        if (request.GET.get('opened')):
            currentTime = datetime.now().time()
            context['time'] = currentTime
        context['distance'] = max_distance
        context['geo_point'] = point
        if max_distance<=100:
            context['zoom'] = 15
            context['unit'] = 'm'
        if max_distance<=250:
            context['zoom'] = 14
            context['unit'] = 'm'
        if max_distance<=500:
            context['zoom'] = 13
            context['unit'] = 'm'
        if max_distance<=999:
            context['zoom'] = 12
            context['unit'] = 'm'
            return context
        if max_distance<=2500:
            context['zoom'] = 11
            context['unit'] = 'km'
            return context
        if max_distance<=5000:
            context['zoom'] = 10
            context['unit'] = 'km'
            return context
        if max_distance<=10000:
            context['zoom'] = 9
            context['unit'] = 'km'
            return context
        if max_distance<=25000:
            context['zoom'] = 8
            context['unit'] = 'km'
            return context
        if max_distance<=50000:
            context['zoom'] = 7
            context['unit'] = 'km'
            return context
        if max_distance<=100000:
            context['zoom'] = 6
            context['unit'] = 'km'
            return context

class ShopViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows shops to be viewed, created or edited.
    """
    queryset = Shop.objects.all().order_by('-created')
    serializer_class = ShopSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

# class TownViewSet(viewsets.ModelViewSet):
#     """
#     Api endpoint that allows towns to be viewed, created or edited.
#     """
#     queryset = Town.objects.all()
#     serializer_class = TownSerializer
#     permission_classes = [IsAdminOrReadOnly]

# class AutocompleteView(ListAPIView):
#     serializer_class = TownSerializer
#     def get_queryset(self):
#         qs = Town.objects.all()
#         if (self.request.GET.get('town')):
#             qs = qs.filter(town__contains=self.request.GET.get('town'))
#         return qs
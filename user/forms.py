#-*- coding: utf-8 -*-
from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import AuthenticationForm, UsernameField, PasswordResetForm, PasswordChangeForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _
from captcha.fields import ReCaptchaField


class MyAuthenticationForm(AuthenticationForm):
    username = UsernameField(
        error_messages={'required': 'Pole "Nazwa użytkownika" jest wymagane', },
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': True}),
    )
    password = forms.CharField(
        error_messages={'required': 'Pole "Hasło" jest wymagane', },
        label="Password",
        strip=False,
        widget=forms.PasswordInput,
    )
    captcha = ReCaptchaField(attrs={'size': 'normal', 'theme' : 'clean', },
                             error_messages={'required': "Nie uzupełniłeś captcha"})
    error_messages = {
        'invalid_login': (
            'Nazwa użytkownika i hasło się nie zgadzają lub konto jest nieaktywne'
        ),
        'inactive': "To konto jest nieaktywne",
    }


def forbidden_usernames_validator(value):
    forbidden_usernames = ['admin', 'settings', 'news', 'about', 'help',
                           'signin', 'signup', 'signout', 'terms', 'privacy',
                           'cookie', 'new', 'login', 'logout', 'administrator',
                           'join', 'account', 'username', 'root', 'blog',
                           'user', 'users', 'billing', 'subscribe', 'reviews',
                           'review', 'blog', 'blogs', 'edit', 'mail', 'email',
                           'home', 'job', 'jobs', 'contribute', 'newsletter',
                           'shop', 'profile', 'register', 'auth',
                           'authentication', 'campaign', 'config', 'delete',
                           'remove', 'forum', 'forums', 'download',
                           'downloads', 'contact', 'blogs', 'feed', 'feeds',
                           'faq', 'intranet', 'log', 'registration', 'search',
                           'explore', 'rss', 'support', 'status', 'static',
                           'media', 'setting', 'css', 'js', 'follow',
                           'activity', 'questions', 'articles', 'network', ]

    if value.lower() in forbidden_usernames:
        raise ValidationError('Zakazana nazwa użytkownika. ')

def invalid_username_validator(value):
    if '@' in value or '+' in value or '-' in value:
        raise ValidationError('Podaj prawidłową nazwę użytkownika.')

def unique_email_validator(value):
    if User.objects.filter(email__iexact=value).exists():
        raise ValidationError('Ten email jest już w naszej bazie.')

def unique_username_ignore_case_validator(value):
    if User.objects.filter(username__iexact=value).exists():
        raise ValidationError('Nazwa uzytkownika jest już zajęta.')


class SignUpForm(forms.ModelForm):
    username = forms.CharField(
        error_messages={'required': 'Pole "Nazwa użytkownika" jest wymagane'},
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        label="Nazwa użytkownika",
        max_length=30,
        required=True,
        help_text='Nazwa użytkownika może zawierać znaki <strong>[a-z]</strong> i znak "<strong>.</strong>"')

    password = forms.CharField(
        error_messages={'required': 'Pole "Hasło" jest wymagane'},
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        label="Hasło")
    confirm_password = forms.CharField(
        error_messages={'required': 'Pole "Wpisz hasło jeszcze raz" jest wymagane'},
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        label="Wpisz hasło jeszcze raz",
        required=True,
        )
    email = forms.CharField(
        error_messages={'required': 'Pole "email" jest wymagane', 'invalid': 'Podaj prawidłowy adres email'},
        widget=forms.EmailInput(attrs={'class': 'form-control'}),
        required=True,
        max_length=75)
    captcha = ReCaptchaField(attrs={'size': 'normal', 'theme' : 'clean', },
                             error_messages={'required': "Nie uzupełniłeś captcha"})
    class Meta:
        model = User
        exclude = ['last_login', 'date_joined']
        fields = ['username', 'email', 'password', 'confirm_password', ]

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.fields['username'].validators.append(forbidden_usernames_validator)
        self.fields['username'].validators.append(invalid_username_validator)
        self.fields['username'].validators.append(
            unique_username_ignore_case_validator)
        self.fields['email'].validators.append(unique_email_validator)

    def clean(self):
        super(SignUpForm, self).clean()
        password = self.cleaned_data.get('password')
        confirm_password = self.cleaned_data.get('confirm_password')
        if password and password != confirm_password:
            self._errors['password'] = self.error_class(
                ['Hasła nie są identyczne'])
        return self.cleaned_data

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class PasswordResetForm2(PasswordResetForm):
    email = forms.EmailField(label=("Email"),
                             max_length=254,
                             error_messages={'required':'pole "Adres email" jest wymagane'},
                             )


class PasswordChangeForm2(PasswordChangeForm):
    old_password = forms.CharField(
        label=_("Old password"),
        strip=False,
        widget=forms.PasswordInput(attrs={'autofocus': True}),
        error_messages={'required':'pole "Stare hasło" jest wymagane'})

    new_password1 = forms.CharField(
        label=_("New password"),
        widget=forms.PasswordInput,
        strip=False,
        help_text=password_validation.password_validators_help_text_html(),
        error_messages={'required':'pole "Nowe hasło" jest wymagane'})

    new_password2 = forms.CharField(
        label=_("New password confirmation"),
        strip=False,
        widget=forms.PasswordInput,
        error_messages={'required': 'pole "Powtórz hasło" jest wymagane'},
    )
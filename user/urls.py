from django.urls import path

from . import views

urlpatterns = [
    path('logowanie/', views.LoginView2.as_view(), name='login'),
    path('wyloguj/', views.LogoutView2.as_view(), name='logout'),
    path('rejestracja/', views.RegisterView.as_view(), name='register'),
    path('zmiana-hasla/', views.PasswordChangeView2.as_view(), name='password_change'),
    path('zmiana-hasla-ukonczona/', views.PasswordChangeDoneView2.as_view(), name='password_change_done'),
    path('user-nav/', views.UserNavView.as_view(), name='user_nav'),
    path('przypomnij-haslo/', views.PasswordResetView2.as_view(), name='password_reset'),
    path('email-z-informacjami-wyslany/', views.PasswordResetDoneView2.as_view(), name='password_reset_done'),
    path('generuj-haslo/<uidb64>/<token>/', views.PasswordResetConfirmView2.as_view(), name='password_reset_confirm'),
    path('generuj-haslo/zrobione/', views.PasswordResetCompleteView2.as_view(), name='password_reset_complete'),
    path('sprawdz/username/<username>/', views.CheckUsername.as_view(), name='check_username'),
    path('sprawdz/mail/<email>/', views.CheckMail.as_view(), name='check_mail')
]

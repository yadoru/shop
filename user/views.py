#-*- coding: utf-8 -*-
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordResetView, PasswordResetCompleteView, \
    PasswordResetConfirmView, PasswordResetDoneView, \
    PasswordChangeDoneView, PasswordChangeView
from django.contrib.messages.views import SuccessMessageMixin
from django.http import JsonResponse
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.generic import CreateView, TemplateView
from rest_framework import viewsets, permissions
from rest_framework.generics import RetrieveAPIView

from common.permissions import IsAdminOrReadOnly
from user.forms import PasswordChangeForm2
from user.models import UserSerializer
from .forms import SignUpForm, MyAuthenticationForm, PasswordResetForm2
from common.mixins import AjaxableResponseMixin, AjaxTemplateMixin, LoginRequiredAjaxMixIn


class LoginView2(SuccessMessageMixin, AjaxTemplateMixin, AjaxableResponseMixin, LoginView):
    template_name = 'user/login.html'
    success_message = "Zostałeś zalogowany"
    form_class = MyAuthenticationForm



class RegisterView(SuccessMessageMixin, AjaxTemplateMixin, AjaxableResponseMixin, CreateView):
    form_class = SignUpForm
    template_name = 'user/register.html'
    success_message = "Zostałeś zarejestrowany"

    def get_success_url(self):
        return reverse('login')


class UserNavView(AjaxTemplateMixin, TemplateView):
    template_name = 'user_nav.html'

class LogoutView2(SuccessMessageMixin, LoginRequiredAjaxMixIn, AjaxTemplateMixin, LogoutView):
    template_name = 'user/logged_out.html'
    success_message = "Zostałeś wylogowany"

    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        #TODO: problem resolved with spaghetti code, find a better way to avoid code repetition in future
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        if self.request.is_ajax():
            logout(request)
            return JsonResponse({"message":"Zostałeś pomyślnie wylogowany"})
        return super().dispatch(request, *args, **kwargs)


class PasswordResetView2(AjaxTemplateMixin, AjaxableResponseMixin, PasswordResetView):
    email_template_name = 'user/password_reset_email.html'
    template_name = 'user/password_reset_form.html'
    success_message = 'Instrukcja pozwalająca ustawić nowe hasło dla podanego adresu email została wysłana. ' \
                      'Niebawem powinna się pojawić na Twoim koncie pocztowym. ' \
                      'W przypadku nieotrzymania wiadomości email upewnij się czy adres wprowadzony jest zgodny ' \
                      'z tym podanym podczas rejestracji i sprawdź zawartość folderu SPAM na swoim koncie.'
    form_class = PasswordResetForm2



class PasswordResetConfirmView2(SuccessMessageMixin, AjaxTemplateMixin, AjaxableResponseMixin, PasswordResetConfirmView):
    template_name = 'user/password_reset_confirm.html'
    success_message = "hasło zostało zmienione"
    post_reset_login = True


class PasswordResetCompleteView2(AjaxTemplateMixin, AjaxableResponseMixin, PasswordResetCompleteView):
    pass


class PasswordResetDoneView2(SuccessMessageMixin, AjaxTemplateMixin, AjaxableResponseMixin, PasswordResetDoneView):
    template_name = 'user/password_reset_done.html'


class PasswordChangeView2(SuccessMessageMixin, LoginRequiredAjaxMixIn, AjaxTemplateMixin, AjaxableResponseMixin,
                          PasswordChangeView):
    template_name = 'user/password_change_form.html'
    form_class = PasswordChangeForm2
    success_message = "hasło zostało pomyślnie zmienione"

class PasswordChangeDoneView2(AjaxTemplateMixin, AjaxableResponseMixin, PasswordChangeDoneView):
    pass

class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [permissions.IsAdminUser]

class CheckMail(RetrieveAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [permissions.AllowAny]
    lookup_field = 'email'

class CheckUsername(RetrieveAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [permissions.AllowAny]
    lookup_field = 'username'

from django.db import models
from django.contrib.auth.models import User
from rest_framework import serializers

from map.models import ShopSerializer


class Profile(models.Model):
    user = models.ForeignKey(User, related_name='profil', on_delete=models.CASCADE, verbose_name='profil')


class UserSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'shops',)
        read_only_fields = ('shops',)
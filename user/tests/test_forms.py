from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.test import TestCase
from ..forms import MyAuthenticationForm, SignUpForm, PasswordResetForm2

class SetupClass(TestCase):
    def setUp(self):
        self.adminUser = User.objects.create_user(username='maciek', password='Aezakmi16', email='admin@mail.com')
        self.inactiveUser = User.objects.create_user(username='inactiveuser', password='Strong30', email='inactive@mail.com')
        self.inactiveUser.is_active=False
        self.inactiveUser.save()
        self.normalUser = User.objects.create_user(username='testuser', password='Strong20', email='normal@mail.com')

class MyAuthenticationFormTest(SetupClass):

    def test_MyAuthenticationForm_valid(self):
        form = MyAuthenticationForm(data={'username':'maciek', 'password':'Aezakmi16'})
        maciek = authenticate(username='maciek', password='Aezakmi16')
        self.assertEqual(form.is_valid(), True)

    def test_MyAuthenticationForm_inactive(self):
        form = MyAuthenticationForm(data={'username':'inactiveuser', 'password':'Strong30'})
        self.assertFalse(form.is_valid())


    def test_MyAuthenticationForm_invalid_login(self):
        form = MyAuthenticationForm(data={'username':'adminuser', 'password':'InvalidPassword'})
        self.assertFalse(form.is_valid())


class SignUpFormTest(TestCase):
    def test_SignUpForm_valid(self):
        form = SignUpForm(data={'username':'marek', 'email':'jaeger@jack.com',
                                'password':'radiance12', 'confirm_password':'radiance12'})
        self.assertTrue(form.is_valid())
    def test_SignUpForm_Empty(self):
        form = SignUpForm(data={'username':'', 'email':'',
                                'password':'', 'confirm_password':''})
        self.assertFalse(form.is_valid())
    def test_SignUpForm_NotProperMail(self):
        form = SignUpForm(data={'username':'marek', 'email':'jaegerjack.com',
                                'password':'radiance12', 'confirm_password':'radiance12'})
        self.assertFalse(form.is_valid())
    def test_SignUpForm_NoPassword(self):
        form = SignUpForm(data={'username':'marek', 'email':'jaeger@jack.com',
                                'password':'', 'confirm_password':'radiance12'})
        self.assertFalse(form.is_valid())
    def test_SignUpForm_InvalidUsername(self):
        form = SignUpForm(data={'username':'marek+', 'email':'jaeger@jack.com',
                                'password':'radiance12', 'confirm_password':'radiance12'})
        self.assertFalse(form.is_valid())
    def test_SignUpForm_ForbiddenUsername(self):
        form = SignUpForm(data={'username':'admin', 'email':'jaeger@jack.com',
                                'password':'radiance12', 'confirm_password':'radiance12'})
        self.assertFalse(form.is_valid())
    def test_SignUpForm_EmptyEmail(self):
        form = SignUpForm(data={'username':'marek', 'email':'',
                                'password':'radiance12', 'confirm_password':'radiance12'})
        self.assertFalse(form.is_valid())
    def test_SignUpForm_EmptyPasswords(self):
        form = SignUpForm(data={'username':'marek', 'email':'jaeger@jack.com',
                                'password':'', 'confirm_password':''})
        self.assertFalse(form.is_valid())
    def test_SignUpForm_EmptyConfirmPassword(self):
        form = SignUpForm(data={'username':'marek', 'email':'jaeger@jack.com',
                                'password':'radiance12', 'confirm_password':''})
        self.assertFalse(form.is_valid())
    def test_SignUpForm_EmptyUsernamePassword(self):
        form = SignUpForm(data={'username':'', 'email':'jaeger@jack.com',
                                'password':'', 'confirm_password':'radiance12'})
        self.assertFalse(form.is_valid())
    def test_SignUpForm_EmptyUsernameBothPasswords(self):
        form = SignUpForm(data={'username':'', 'email':'jaeger@jack.com',
                                'password':'', 'confirm_password':''})
        self.assertFalse(form.is_valid())
    def test_SignUpForm_EmptyUsernameConfirmPassword(self):
        form = SignUpForm(data={'username':'', 'email':'jaeger@jack.com',
                                'password':'radiance12', 'confirm_password':''})
        self.assertFalse(form.is_valid())
    def test_SignUpForm_EmptyMailPassword(self):
        form = SignUpForm(data={'username':'marek', 'email':'',
                                'password':'', 'confirm_password':'radiance12'})
        self.assertFalse(form.is_valid())
    def test_SignUpForm_EmptyMailConfirmPassword(self):
        form = SignUpForm(data={'username':'marek', 'email':'',
                                'password':'radiance12', 'confirm_password':''})
        self.assertFalse(form.is_valid())
    def test_SignUpForm_EmptyMailBothPasswords(self):
        form = SignUpForm(data={'username':'marek', 'email':'',
                                'password':'', 'confirm_password':''})
        self.assertFalse(form.is_valid())


class PasswordResetForm2Test(TestCase):
    def test_PasswordResetForm2_valid(self):
        form = PasswordResetForm2(data={'email':'jaegerjack16@gmail.com'})
        self.assertTrue(form.is_valid())
    def test_PasswordResetForm2_InvalidMailFormat(self):
        form = PasswordResetForm2(data={'email':'jaegerjack16gmail.com'})
        self.assertFalse(form.is_valid())

from django.contrib.auth.models import User
from django.urls import reverse

from django.test import TestCase, Client

#unlogged client for testing of non authenticated views
c = Client()

class LoginView2Test(TestCase):
    def setUp(self):
        user = User.objects.create_user('testuser','test@gmail.com','Kakegurui18',)
        inactive_user = User.objects.create_user('inactiveuser', 'test@mail.com', 'Inactive18')
        inactive_user.is_active=False
        inactive_user.save()

    def test_LoginView2_get(self):
        response = c.get(reverse('login'))
        self.assertEqual(response.status_code, 200)

    def test_LoginView2_getAjax(self):
        response = c.get(reverse("login"), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

    def test_LoginView2_postSuccess(self):
        response = c.post(reverse("login"), {'username':'testuser', 'password':'Kakegurui18'})
        self.assertEqual(response.url, '/')

    def test_LoginView2_postAjaxSuccess(self):
        response = c.post(reverse("login"), {'username':'testuser', 'password':'Kakegurui18'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, "ok")

    def test_LoginView2_postInactive(self):
        response = c.post(reverse("login"), {'username':'inactiveuser', 'password':'Inactive18'})
        self.assertEqual(response.status_code, 200)

    def test_LoginView2_postAjaxInactive(self):
        response = c.post(reverse("login"), {'username':'inactiveuser', 'password':'Inactive18'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_LoginView2_postError(self):
        response = c.post(reverse("login"), {'username':'testuser', 'password':'Kakegurui15'})
        self.assertEqual(response.status_code, 200)

    def test_LoginView2_postAjaxError(self):
        response = c.post(reverse("login"), {'username':'testuser', 'password':'Kakegurui15'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_LoginView2_postEmpty(self):
        response = c.post(reverse("login"), {'username':'', 'password':''})
        self.assertEqual(response.status_code, 200)

    def test_LoginView2_postAjaxEmpty(self):
        response = c.post(reverse("login"), {'username':'testuser', 'password':'Kakegurui15'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_LoginView2_postEmptyLogin(self):
        response = c.post(reverse("login"), {'username':'', 'password':'Kakegurui15'})
        self.assertEqual(response.status_code, 200)

    def test_LoginView2_postAjaxEmptyLogin(self):
        response = c.post(reverse("login"), {'username':'', 'password':'Kakegurui15'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_LoginView2_postEmptyPassword(self):
        response = c.post(reverse("login"), {'username':'testuser', 'password':''})
        self.assertEqual(response.status_code, 200)

    def test_LoginView2_postAjaxEmptyPassword(self):
        response = c.post(reverse("login"), {'username':'testuser', 'password':''},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)


class RegisterViewTest(TestCase):
    def setUp(self):
        user = User.objects.create_user('desolator11', 'jaeger11@mail.com', 'Ateish16')

    def test_RegisterView2_get(self):
        response = c.get(reverse('register'))
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_getAjax(self):
        response = c.get(reverse("register"), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postSuccess(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'jaeger@mail.com',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'})
        self.assertEqual(response.status_code, 302)

    def test_RegisterView2_postAjaxSuccess(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'jaeger@mail.com',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, "ok")

    def test_RegisterView2_postNotEqualError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'jaeger@mail.com',
                                                'password':'Ateish15', 'confirm_password':'Ateish16'})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxNotEqualError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'jaeger@mail.com',
                                                'password':'Ateish15', 'confirm_password':'Ateish16'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_postForbiddenNameError(self):
        response = c.post(reverse("register"), {'username':'admin', 'email':'jaeger@mail.com',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxForbiddenNameError(self):
        response = c.post(reverse("register"), {'username':'admin', 'email':'jaeger@mail.com',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_postInvalidUsernameError(self):
        response = c.post(reverse("register"), {'username':'desolator+', 'email':'jaeger@mail.com',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxInvalidUsernameError(self):
        response = c.post(reverse("register"), {'username':'desolator+', 'email':'jaeger@mail.com',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_ExistingMailError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'jaeger11@mail.com',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxExistingMailError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'jaeger11@mail.com',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_postExistingUserError(self):
        response = c.post(reverse("register"), {'username':'desolator11', 'email':'jaeger@mail.com',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxExistingUserError(self):
        response = c.post(reverse("register"), {'username':'desolator11', 'email':'jaeger@mail.com',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_postEmptyLoginError(self):
        response = c.post(reverse("register"), {'username':'', 'email':'jaeger@mail.com',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxEmptyLoginError(self):
        response = c.post(reverse("register"), {'username':'', 'email':'jaeger@mail.com',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_postEmptyLoginPasswordError(self):
        response = c.post(reverse("register"), {'username':'', 'email':'jaeger@mail.com',
                                                'password':'', 'confirm_password':'Ateish16'})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxEmptyLoginPasswordError(self):
        response = c.post(reverse("register"), {'username':'', 'email':'jaeger@mail.com',
                                                'password':'', 'confirm_password':'Ateish16'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_postEmptyLoginConfirmPasswordError(self):
        response = c.post(reverse("register"), {'username':'', 'email':'jaeger@mail.com',
                                                'password':'Ateish16', 'confirm_password':''})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxEmptyLoginConfirmPasswordError(self):
        response = c.post(reverse("register"), {'username':'', 'email':'jaeger@mail.com',
                                                'password':'Ateish16', 'confirm_password':''},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_postEmptyLoginBothPasswordsError(self):
        response = c.post(reverse("register"), {'username':'', 'email':'jaeger@mail.com',
                                                'password':'', 'confirm_password':''})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxEmptyLoginBothPasswordsError(self):
        response = c.post(reverse("register"), {'username':'', 'email':'jaeger@mail.com',
                                                'password':'', 'confirm_password':''},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_postEmptyError(self):
        response = c.post(reverse("register"), {'username':'', 'email':'',
                                                'password':'', 'confirm_password':''},)
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxEmptyError(self):
        response = c.post(reverse("register"), {'username':'', 'email':'',
                                                'password':'', 'confirm_password':''},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_postEmptyMailError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxEmptyMailError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_postEmptyMailPasswordError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'',
                                                'password':'', 'confirm_password':'Ateish16'})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxEmptyMailPasswordError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'',
                                                'password':'', 'confirm_password':'Ateish16'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_postEmptyMailConfirmPasswordError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'',
                                                'password':'Ateish16', 'confirm_password':''})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxEmptyMailConfirmPasswordError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'',
                                                'password':'Ateish16', 'confirm_password':''},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_postEmptyMailBothPasswordsError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'',
                                                'password':'', 'confirm_password':''})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxEmptyMailBothPasswordsError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'',
                                                'password':'', 'confirm_password':''},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)


    def test_RegisterView2_postEmptyPasswordError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'jaeger@mail.com',
                                                'password':'', 'confirm_password':'Ateish16'})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxEmptyPasswordError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'jaeger@mail.com',
                                                'password':'', 'confirm_password':'Ateish16'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_postEmptyConfirmPasswordError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'jaeger@mail.com',
                                                'password':'Ateish16', 'confirm_password':''})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxEmptyConfirmPasswordError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'jaeger@mail.com',
                                                'password':'Ateish16', 'confirm_password':''},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_postEmptyBothPasswordsError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'jaeger@mail.com',
                                                'password':'', 'confirm_password':''})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxEmptyBothPasswordsError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'jaeger@mail.com',
                                                'password':'', 'confirm_password':''},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_RegisterView2_postInvalidMailFormatError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'jaegermail.com',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'})
        self.assertEqual(response.status_code, 200)

    def test_RegisterView2_postAjaxInvalidMailFormatError(self):
        response = c.post(reverse("register"), {'username':'desolator', 'email':'jaegermail.com',
                                                'password':'Ateish16', 'confirm_password':'Ateish16'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)



class LogoutView2Test(TestCase):
    loggedC = None
    def setUp(self):
        # Logged in client for testing of users which require being logged in
        # Here, we reset our client so we are assured that each test starts with logged in client
        clientUser = User.objects.create_user('loggedC', 'client@mail.com', 'Client16')
        self.loggedC = Client()
        self.loggedC.login(username='loggedC', password='Client16')

    def test_LogoutView2_getUnloggedError(self):
        response = c.get(reverse('logout'))
        self.assertEqual(response.status_code, 302)

    def test_LogoutView2_getAjaxUnloggedError(self):
        response = c.get(reverse("logout"), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_LogoutView2_getLogged(self):
        response = self.loggedC.get(reverse('logout'))
        self.assertEqual(response.status_code, 200)

    def test_LogoutView2_getAjaxLogged(self):
        response = self.loggedC.get(reverse('logout'), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)


class PasswordResetView2Test(TestCase):
    def setUp(self):
        user = User.objects.create_user('jacek', 'jacek@mail.com', 'Masztalewicz37')

    def test_PasswordResetView2_get(self):
        response = c.get(reverse('password_reset'))
        self.assertEqual(response.status_code, 200)

    def test_PasswordResetView2_getAjax(self):
        response = c.get(reverse('password_reset'), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

    def test_PasswordResetView2_postSuccess(self):
        response = c.post(reverse('password_reset'), {'email': 'jacek@mail.com'})
        self.assertEqual(response.status_code, 302)

    def test_PasswordResetView2_postAjaxSuccess(self):
        response = c.post(reverse('password_reset'), {'email': 'jacek@mail.com'},
                          HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, 'ok')

    def test_PasswordResetView2_postEmailNotInDatabaseBehaviour(self):
        #if mail is not in database it will still show success message.
        response = c.post(reverse('password_reset'), {'email': 'jacek30@mail.com'})
        self.assertEqual(response.status_code, 302)

    def test_PasswordResetView2_postAjaxEmailNotInDatabaseBehaviour(self):
        response = c.post(reverse('password_reset'), {'email': 'jacek20@mail.com'},
                         HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, 'ok')

    def test_PasswordResetView2_postEmptyError(self):
        response = c.post(reverse('password_reset'))
        self.assertEqual(response.status_code, 200)

    def test_PasswordResetView2_postAjaxEmptyError(self):
        response = c.post(reverse('password_reset'), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_PasswordResetView2_postInvalidMailFormatError(self):
        response = c.post(reverse('password_reset'), {'email':'kot.com'})
        self.assertEqual(response.status_code, 200)

    def test_PasswordResetView2_postAjaxInvalidMailFormatError(self):
        response = c.post(reverse('password_reset'), {'email':'dom.com'} , HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)


class PasswordChangeView2Test(TestCase):
    loggedC = None
    def setUp(self):
        user = User.objects.create_user('arek', 'arek@mail.com', 'arkadiusz13')
        self.loggedC = Client()
        self.loggedC.login(username='arek', password='arkadiusz13')
    def test_PasswordCHangeView2_UnloggedError(self):
        response = c.get(reverse('password_change'))
        self.assertEqual(response.status_code, 302)

    def test_PasswordChangeView2_getAjaxUnloggedError(self):
        response = c.get(reverse("password_change"), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_PasswordChangeView2_get(self):
        response = self.loggedC.get(reverse('password_change'))
        self.assertEqual(response.status_code, 200)

    def test_PasswordChangeView2_getAjax(self):
        response = self.loggedC.get(reverse('password_change'), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

    def test_PasswordChangeView2_postEmptyError(self):
        response = self.loggedC.post(reverse('password_change'))
        self.assertEqual(response.status_code, 200)

    def test_PasswordChangeView2_postAjaxEmptyError(self):
        response = self.loggedC.post(reverse('password_change'), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_PasswordChangeView2_postSuccess(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password':'arkadiusz13',
                                                                  'new_password1':'mokasyny',
                                                                  'new_password2':'mokasyny'})
        self.assertEqual(response.status_code, 302)

    def test_PasswordChangeView2_postAjaxSuccess(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password':'arkadiusz13',
                                                                  'new_password1':'mokasyny',
                                                                  'new_password2':'mokasyny'},
                                     HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, 'ok')

    def test_PasswordChangeView2_postOldPasswordError(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password':'arkadiusz12',
                                                                  'new_password1':'mokasyny',
                                                                  'new_password2':'mokasyny'})
        self.assertEqual(response.status_code, 200)

    def test_PasswordChangeView2_postAjaxOldPasswordError(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password':'arkadiusz12',
                                                                  'new_password1':'mokasyny',
                                                                  'new_password2':'mokasyny'},
                                     HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_PasswordChangeView2_postEmptyOldPasswordError(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password':'',
                                                                  'new_password1':'mokasyny',
                                                                  'new_password2':'mokasyny'})
        self.assertEqual(response.status_code, 200)

    def test_PasswordChangeView2_postAjaxEmptyOldPasswordError(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password':'',
                                                                  'new_password1':'mokasyny',
                                                                  'new_password2':'mokasyny'},
                                     HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_PasswordChangeView2_postNotEqualPassowrdsError(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password': 'arkadiusz13',
                                                                  'new_password1': 'mokasyny',
                                                                  'new_password2': 'mokasyn'})
        self.assertEqual(response.status_code, 200)

    def test_PasswordChangeView2_postAjaxNotEqualPassowrdsError(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password': 'arkadiusz13',
                                                                  'new_password1': 'mokasyny',
                                                                  'new_password2': 'mokasyn'},
                                     HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_PasswordChangeView2_postEmptyNewPasswordError(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password': 'arkadiusz13',
                                                                  'new_password1': '',
                                                                  'new_password2': 'mokasyny'})
        self.assertEqual(response.status_code, 200)

    def test_PasswordChangeView2_postAjaxEmptyNewPasswordError(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password': 'arkadiusz13',
                                                                  'new_password1': '',
                                                                  'new_password2': 'mokasyny'},
                                     HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_PasswordChangeView2_postEmptyConfirmPasswordError(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password': 'arkadiusz13',
                                                                  'new_password1': 'mokasyny',
                                                                  'new_password2': ''})
        self.assertEqual(response.status_code, 200)

    def test_PasswordChangeView2_postAjaxEmptyConfirmPasswordError(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password': 'arkadiusz13',
                                                                  'new_password1': 'mokasyny',
                                                                  'new_password2': ''},
                                     HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_PasswordChangeView2_postEmptyOldPasswordNewPasswordError(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password': '',
                                                                  'new_password1': '',
                                                                  'new_password2': 'mokasyny'})
        self.assertEqual(response.status_code, 200)

    def test_PasswordChangeView2_postAjaxEmptyOldPasswordNewPasswordError(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password': '',
                                                                  'new_password1': '',
                                                                  'new_password2': 'mokasyny'},
                                     HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

    def test_PasswordChangeView2_postEmptyOldPasswordConfirmPasswordError(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password': '',
                                                                  'new_password1': 'mokasyny',
                                                                  'new_password2': ''})
        self.assertEqual(response.status_code, 200)

    def test_PasswordChangeView2_postAjaxEmptyError(self):
        response = self.loggedC.post(reverse('password_change'), {'old_password': '',
                                                                  'new_password1': 'mokasyny',
                                                                  'new_password2': ''},
                                     HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

class MyShopsViewTest(TestCase):
    def test_MyShopsViewget(self):
        pass
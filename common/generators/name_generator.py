import random
import string
import sys

VOWELS = "aeiou"
CONSONANTS = "".join(set(string.ascii_lowercase) - set(VOWELS))
EXTRA_CONSONANTS = ['sh', 'ch', 'tch', 'dz', 'sz', 'th', 'nn', 'nd', 'bl', 'nt']
LETTERS = "".join(set(string.ascii_lowercase))

def generate_word(length):
    word = ""+random.choice(CONSONANTS + VOWELS)
    for i in range(1, length):
        if word[i-1] in VOWELS:
            word += random.choice(EXTRA_CONSONANTS + list(CONSONANTS))
        else:
            word += random.choice(VOWELS)
    if length<len(word):
        for i in range(length, len(word)):
            word=word[:-1]
    return word

def generate_sentence(words, limit=100):
    sentence = ''
    for i in range(words):
        length = random.choice(range(0,8))
        sentence +=generate_word(length)
        if len(sentence)>=limit:
            for x in range(limit, len(sentence)):
                sentence=sentence[:-1]
            break
        if(i!=words-1):
            sentence+=' '
    return sentence
            
if __name__ == "__main__":
    try:
        count = int(sys.argv[1])
    except:
        count = 5

    try:
        length = int(sys.argv[2])
    except:
        length = 6

    for i in range(count):
        print(generate_sentence(length))
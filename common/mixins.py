from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy


class AjaxableRedirectOnGetMixIn:
    send_to = None
    def get(self, request,*args, **kwargs):
        if self.request.is_ajax():
            return JsonResponse({'redirect':reverse_lazy(self.send_to)})
        return redirect(reverse_lazy(self.send_to))

class AjaxableSelfRedirectOnPostMixIn:
    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            return JsonResponse({'redirect':self.request.get_full_path()})
        return redirect(request.get_full_path())

class AjaxableResponseMixin:
    message = ''
    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        response = super().form_valid(form)
        if self.request.is_ajax():
            data = {
                'ok' : 'ok',
            }
            if self.message:
                data.update({'message': self.message})
            try:
                data.update({'pk': self.object.pk})
            except AttributeError:
                pass
            return JsonResponse(data)
        else:
            return response

class AjaxTemplateMixin:

    def pick_template(self):
        base = 'base.html'
        if self.request.is_ajax():
            base = 'base_ajax.html'
        return base

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['base'] = self.pick_template()
        return context

class LoginRequiredAjaxMixIn(LoginRequiredMixin):
#Handles both normal and ajax requests for views which require Login
#If user is not authenticated sends either JsonResponse or redirects to login
    permission_denied_message = 'Ta akcja wymaga zalogowania'

    def handle_no_permission(self):
        if self.request.is_ajax():
            data = {
                'redirect': self.get_login_url(),
                'error': self.permission_denied_message,
            }
            return JsonResponse(data, status=400)
        messages.error(self.request, self.permission_denied_message)
        return super().handle_no_permission()
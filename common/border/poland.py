import json

from django.contrib.gis.geos import GEOSGeometry

from shop.settings import BASE_DIR

centre = GEOSGeometry('POINT(52.069167 19.480556)')

def get_border():
    with open(str(BASE_DIR + '/common/border/poland.geojson')) as f:
        data = json.load(f)
        border = GEOSGeometry(str(data['features'][0]['geometry']))
    return border

from django.urls import path

from . import views

urlpatterns = [
    path('moje-sklepy/', views.MyShopsView.as_view(), name='my_shops'),
    path('dodaj-sklep/', views.AddUsingGoogleMapsView.as_view(), name='map_based'),
#    path('google/reverse_geocoding/', views.ReverseGeocodingView.as_view(), name='reverse_geocoding'),
]
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse

c = Client()
class MyShopsViewTest(TestCase):
    loggedC = None
    user = User.objects.create_user('testy', 'crunchy@mail.com', 'kotlety99')
    def setUp(self):
        self.loggedC = Client()
        self.loggedC.login(username='testy', password='kotlety99')
    def test_MyShopsView_getUnlogged(self):
        response = c.get(reverse('my_shops'))
        self.assertEqual(response.status_code, 302)
    def test_MyShopsView_getAjaxUnlogged(self):
        response = c.get(reverse('my_shops'), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

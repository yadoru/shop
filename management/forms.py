from django import forms
from django.core.exceptions import ValidationError

from map.models import Shop

from mapwidgets.widgets import GooglePointFieldWidget
from captcha.fields import ReCaptchaField


class ShopForm(forms.ModelForm):
    class Meta:
        model = Shop
        # fields = ['opening_hour', 'closing_hour', 'name', 'geo_point', 'address', 'photo']
        fields = ['opening_hour', 'closing_hour', 'name', 'geo_point', 'photo']
        widgets = {
            'geo_point': GooglePointFieldWidget,
        }
        error_messages = {
            'geo_point':{
                'invalid' : 'Nieprawidłowy punkt na mapie',
                'required' : 'Nie wybrałeś żadnego punktu na mapie',
            },
            'address':{
                'required' : 'Nie byliśmy w stanie utworzyć adresu',
                'invalid' : 'Nastąpił niespodziewany błąd(RRoD, Error E01)',
            }
        }

    name = forms.CharField(label='Nazwa sklepu', error_messages={'required': 'Pole "Nazwa sklepu" jest wymagane'})

    opening_hour = forms.TimeField(label='Godzina otwarcia sklepu',
                                   error_messages={'invalid':'Wprowadź poprawną godzinę otwarcia sklepu',
                                                   'required': 'Pole "Godzina otwarcia sklepu" jest wymagane'},
                                   input_formats=['%H:%M'],
                                   widget=forms.TimeInput(format='%H:%M'))

    closing_hour = forms.TimeField(label='Godzina zamknięcia sklepu',
                                   error_messages={'invalid':'Wprowadź poprawną godzinę zamknięcia sklepu',
                                                   'required': 'Pole "Godzina zamknięcia sklepu" jest wymagane'},
                                   input_formats=['%H:%M'],
                                   widget=forms.TimeInput(format='%H:%M'))
    photo = forms.ImageField(label='Wybierz zdjęcie sklepu',
                               help_text='Zdjęcie powinno ukazywać wejście sklepu.', required=False)

    def clean_photo(self):
        image = self.cleaned_data.get('photo', False)
        if image:
            if image.size > 4 * 1024 * 1024:
                raise ValidationError("Zdjęcie jest zbyt duże(max. 4MB)")
            return image
        else:
            raise ValidationError("Nie byliśmy w stanie przetworzyć zdjęcia")

class ShopCaptchaForm(ShopForm):
    captcha = ReCaptchaField(attrs={'size': 'normal', 'theme' : 'clean', },
                             error_messages={'required': "Nie uzupełniłeś captcha"})
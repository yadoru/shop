# import requests
# from django.http import JsonResponse
from django.urls import reverse
from django.views.generic import TemplateView, FormView, CreateView, ListView
from django.conf import settings
# from django.views.generic.base import View

from . import forms

from common.mixins import LoginRequiredAjaxMixIn, AjaxTemplateMixin, AjaxableResponseMixin
# from map.models import Shop, Town
from map.models import Shop

google_key = settings.GOOGLE_API_KEY


class MyShopsView(LoginRequiredAjaxMixIn, AjaxTemplateMixin, ListView):
    template_name = 'management/my_shops.html'
    model = Shop
    def get_queryset(self):
        qs = Shop.objects.all().filter(owner=self.request.user)
        return qs

class Addshop(LoginRequiredAjaxMixIn ,AjaxTemplateMixin, TemplateView):
    template_name = 'management/addshop.html'

class AddFromCurrentLocationView(AjaxableResponseMixin, LoginRequiredAjaxMixIn, AjaxTemplateMixin, FormView):
    form_class = forms.ShopCaptchaForm
    template_name = 'management/lokacja.html'


class AddUsingGoogleMapsView(AjaxableResponseMixin, LoginRequiredAjaxMixIn, AjaxTemplateMixin, CreateView):
    model = Shop
    message = "Twój sklep został utworzony."
    form_class = forms.ShopCaptchaForm
    template_name = 'management/mapa.html'
    def get_success_url(self):
        return reverse('my_shops')

    def form_valid(self, form):
        # long_adres = form.cleaned_data.get('address')
        # list = long_adres.split(', ')
        #list[-1] is the country, we don't need it
        #list[-2] is the town, we put it into town field
        # form_town = ''.join(list[-2])
        # address = ' '.join(list[:len(list)-2])
        # try:
        #     town_object = Town.objects.get(town=form_town)
        # except Town.DoesNotExist:
        #     town_object = None
        # if(town_object):
        #     form.instance.town = town_object
        # else:
        #     town_object = Town.objects.create(town=form_town)
        #     form.instance.town = town_object
        # form.instance.address = address
        form.instance.owner = self.request.user
        return super().form_valid(form)

# class ReverseGeocodingView(LoginRequiredAjaxMixIn, View):
#     part1 = "https://maps.googleapis.com/maps/api/geocode/json?latlng="
#     part2 = "&language=pl&key="
#     def get(self, request, *args, **kwargs):
#         data = {'error':'Nieprawidłowy sposób użycia'}
#         return JsonResponse(data, status=400)
#
#     def post(self, request, *args, **kwargs):
#         latlng = request.POST['latlng']
#         url = self.part1+latlng+self.part2+google_key
#         serialized_data = requests.get(url).json()
#         full_name = serialized_data['results'][00]['formatted_address']
#         list = full_name.split(',')
#         return JsonResponse({"town":list[-2], 'adres':list[:len(list)-2]})
#!/bin/bash
docker exec django01 python manage.py makemigrations
docker exec django01 python manage.py migrate
docker exec django01 python manage.py collectstatic --noinput
docker exec django01 ./manage.py shell -c "
from django.contrib.auth.models import User;
User.objects.filter(username='admin').delete();
User.objects.create_superuser('admin', 'admin@example.com', 'adminpass');
print('Zrobione! Ze stroną połączysz się przez ip localhost.');
print('Domyślne konto superadministratora to:');
print('login:admin');
print('hasło:adminpass');"
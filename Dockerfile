FROM python:3.6

RUN mkdir /code

WORKDIR /code

ADD requirements.txt /code/

RUN pip install -r requirements.txt
RUN apt-get -qq update
RUN apt-get install -yq binutils libproj-dev gdal-bin git
ADD . /code/